# -*- coding: utf-8 -*-

from smbus import SMBus
from math import pi, atan2
import time, struct, threading
import pynmea2, serial

'''
    Данный класс позволяет получать данные непосредственн с датчиков
    без какой-либо посдедующей фильтрации

    http://wiki.amperka.ru/%D0%BF%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D1%8B:troyka-imu-10-dof#принципиальная_схема
'''
class Sensors:

    # Адрес акселлерометра на i2c шине
    _accel_i2c_address = 0b0011000

    # Адрес гироскопа на i2c шине
    _gyro_i2c_address = 0b1101000

    # Адрес магнитометра на i2c шине
    _mag_i2c_address = 0b0011100

    # Коэффициенты усиления
    _mult_accel = 2 / 32767.0
    _mult_gyro = 0.00875
    _mult_mag = 6842.0

    # Объект для чтения данных по сериал порту (UART) с GPS-антены
    gps = None

    # Объект, поля которого будут представлять данные со спутников
    gps_data = None

    # Объект I2c шины данных
    bus = None

    # Ускорение свободного падения
    G = 9.8

    # Коэффициент перевода градусов в радианы
    DEG_TO_RAD = 0.0175

    def __init__(self):
        self.init_i2c_bus()
        self.init_GPS()

    # Подключение к i2c-шине
    # TODO добавить поток
    def init_i2c_bus(self):
        while not self.bus:
            try:
                self.bus = SMBus(1)
                print "Connection with i2c-bus established"

                self.init_accel()
                self.init_gyro()
                self.init_mag()
                self.init_range()

            except:
                print "Error to connect to i2c-bus, try again"
                time.sleep(0.2)

    # Подключение к GPS-антене
    # TODO добавить поток
    def init_GPS(self):
        while not self.gps:
            try:
                self.gps = serial.Serial('/dev/ttyAMA0', baudrate=9600, timeout=0)
                print "Connection with GPS established"
            except:
                print "Error to connect to GPS, try again"
                time.sleep(0.2)

    # Инициализирует работу акселерометра
    def init_accel(self):
        ctrl_reg1 = 0

        # Включение координат x, y, z
        ctrl_reg1 |= (1 << 0)
        ctrl_reg1 |= (1 << 1)
        ctrl_reg1 |= (1 << 2)

        # Включение акселерометра
        ctrl_reg1 |= (1 << 5)

        self.bus.write_byte_data(self._accel_i2c_address, 0x20, ctrl_reg1)

    # Инициализирует работу гироскопа
    def init_gyro(self):
        ctrl_reg1 = 0

        # Включение координат x, y, z
        ctrl_reg1 |= (1 << 0)
        ctrl_reg1 |= (1 << 1)
        ctrl_reg1 |= (1 << 2)

        # Включение гироскопа
        ctrl_reg1 |= (1 << 3)

        self.bus.write_byte_data(self._gyro_i2c_address, 0x20, ctrl_reg1)

    # Инициализирует работу магнитометра
    def init_mag(self):
        self.bus.write_byte_data(self._mag_i2c_address, 0x22, 0)

    # Устанавливется точность для инерциальных датчиков
    def init_range(self):
        # Точность акселерометра
        self.bus.write_byte_data(self._accel_i2c_address, 0x23, 0x00)

        # Точность гироскопа
        self.bus.write_byte_data(self._gyro_i2c_address, 0x23, 0x00)

        # Точность магнитометра
        self.bus.write_byte_data(self._mag_i2c_address, 0x21, 0x00)

    # Обновляет показания с GPS-антены (В потоке)
    def start_gps_updating(self):

        def clock():
            while(1):
                data = self.gps.readline().decode('ascii', errors='replace')
                if data.find('GGA') > 0:
                    self.gps_data = pynmea2.parse(data)
                    print str(self.gps_data)
                    time.sleep(1)
                else:
                    time.sleep(0.05)

        t = threading.Thread(target=clock)
        t.daemon = True
        t.start()

    # Проверка регистра WH0_AM_I (доступность датчика) акселерометра
    def accel_who_am_i(self):
        return self.bus.read_byte_data(self._accel_i2c_address, 0x0F)

    # Проверка регистра WH0_AM_I (доступность датчика) гироскопа
    def gyro_who_am_i(self):
        return self.bus.read_byte_data(self._gyro_i2c_address, 0x0F)

    # Проверка регистра WH0_AM_I (доступность датчика) магнитометра
    def mag_who_am_i(self):
        return self.bus.read_byte_data(self._mag_i2c_address, 0x0F)

    # Возвращает байт (int) (обертка)
    def read_byte(self, address, reg):
        return self.bus.read_byte_data(address, reg)

    # Возращает вещественное число
    def get_float(self, address, low_byte_reg):
        low_byte = self.read_byte(address, low_byte_reg)
        high_byte = self.read_byte(address, low_byte_reg + 1)
        return struct.unpack("h", struct.pack("BB", low_byte, high_byte))[0]

    # Возвращает линейное ускорение вдоль оси X (м/c^2)
    def get_accel_x(self):
        return self.get_float(self._accel_i2c_address, 0x28) * self._mult_accel * self.G

    # Возвращает линейное ускорение вдоль оси Y (м/c^2)
    def get_accel_y(self):
        return self.get_float(self._accel_i2c_address, 0x2A) * self._mult_accel * self.G

    # Возвращает линейное ускорение вдоль оси Z (м/c^2)
    def get_accel_z(self):
        return self.get_float(self._accel_i2c_address, 0x2C) * self._mult_accel * self.G

    # Возвращает уговую скорость вдоль оси X (рад/c)
    def get_gyro_x_rad(self):
        return self.get_float(self._gyro_i2c_address, 0x28) * self._mult_gyro * self.DEG_TO_RAD

    # Возвращает уговую скорость вдоль оси Y (рад/c)
    def get_gyro_y_rad(self):
        return self.get_float(self._gyro_i2c_address, 0x2A) * self._mult_gyro * self.DEG_TO_RAD

    # Возвращает уговую скорость вдоль оси Z (рад/c)
    def get_gyro_z_rad(self):
        return self.get_float(self._gyro_i2c_address, 0x2C) * self._mult_gyro * self.DEG_TO_RAD

    # Возвращает азимут (угол отклонения от направления на северный полюс)
    def get_azimuth(self):
        x = self.get_float(self._mag_i2c_address, 0x28) / self._mult_mag
        y = self.get_float(self._mag_i2c_address, 0x2A) / self._mult_mag

        heading = atan2(y, x)

        if heading < 0:
            heading += 2 * pi

        if heading > 2 * pi:
            heading -= 2 * pi

        return heading

    def get_GPS_latitude(self):
        if self.gps_data:
            return self.gps_data.lat
        else:
            return 0

    def get_GPS_longitude(self):
        if self.gps_data:
            return self.gps_data.lon
        else:
            return 0
