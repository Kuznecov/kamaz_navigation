# -*- coding: utf-8 -*-

import struct
import socket
import threading

'''
    Класс реализует modbus-slave для функции 03 протокола modbus,
    т.е

    Пример создания объекта:
    m = Modbus()
'''
class Modbus(threading.Thread):

    # Стандартный порт для modbusTCP.
    port = 502

    # Адрес сервера (Для Raspberry нужно указать ip адрес в локальной сети)
    ip_address = "localhost"

    # Поле для хранения сокета
    sock = None

    # Массив данных (пока пустой)
    data = []

    # Длинна массива данных
    data_len = 0

    def __init__(self, data):
        threading.Thread.__init__(self)
        self._init_data(data)
        self._init_socket_server()

    # Создает TCP-сервер (modbus-slave)
    def _init_socket_server(self):
        self.sock = socket.socket()
        self.sock.bind((self.ip_address, self.port))

        self.sock.listen(1)

        print "Modbus-slave is open on %s:%d, " % (self.ip_address, self.port)

        # Запускаем поток (выполнится метод run() )
        self.start()

    # Инициализирует массив данных
    def _init_data(self, data):
        self.data = data
        self.data_len = len(data)

        self.data_len_str = hex(len(data) * 4)[2:4]
        self.message_str = hex(len(data) * 4 + 3)[2:4]

    # Обновляет массив данных
    def update_data(self, data):
        self.data = data

    # Ожидает подключение мастера, затем организует передачу данных.
    # Вызов метода start(), унаследованного от threading.Thread,
    # приводит к вызову данного метода.
    def run(self):
        print "Waiting for master connecting..."

        conn, address = self.sock.accept()
        print "Master is connecting (from %s)" % str(address[0])

        conn.settimeout(30)  # установка таймаута (в секундах)

        while True:
            try:
                received_data = conn.recv(64)

                if not received_data:
                    print("No data from master")
                    break

                if (not received_data[0]) or (not received_data[1]):
                    print("bad data from master")
                    continue

                transaction_id = received_data[0].encode('hex') + \
                                 received_data[1].encode('hex')

                buf = ""
                for i in range(self.data_len):
                    buf += self._encode_data( float(self.data[i]) )

                response_str = transaction_id + '0000' + '00' + \
                           self.message_str  + '0103' + \
                           self.data_len_str   + buf

                #print response_str + '\n'

                response_bytes = bytearray.fromhex(response_str)
                conn.send(response_bytes)

            # Обработка ошибок подключения (например, разрыв соединения мастером).
            except socket.error as msg:
                print "Soket error: %s" % msg
                break

        print "Master disconnected."

        # Закрываем соединение и снова ждем подключение мастера.
        conn.close()
        self.run()

    def _encode_data(self, data_float):
        x = hex(struct.unpack("I", struct.pack("f", data_float))[0])

        if x == '0x0':
            x = '0x00000000'
        return x[2:10]

