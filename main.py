# -*- coding: utf-8 -*-

from modbus_tcp import Modbus
from sensors import Sensors
import time

State = [0] * 15

m = Modbus(State)
sensors = Sensors()
sensors.start_gps_updating()
dt = 0.02

while 0:
    State[0] = sensors.get_accel_x()
    State[1] = sensors.get_accel_y()
    State[2] = sensors.get_accel_z()
    State[3] = sensors.get_gyro_x_rad()
    State[4] = sensors.get_gyro_y_rad()
    State[5] = sensors.get_gyro_z_rad()
    State[6] = sensors.get_azimuth()

    State[7] = State[0] * dt
    State[8] = State[1] * dt
    State[9] = State[2] * dt

    State[10] = State[3] * dt
    State[11] = State[4] * dt
    State[12] = State[5] * dt

    State[13] = sensors.get_GPS_latitude()
    State[14] = sensors.get_GPS_longitude()

    time.sleep(dt)
